secml.ml.kernel
===============

.. automodule:: secml.ml.kernel
   :members:
   :undoc-members:
   :show-inheritance:


CKernel
-------

.. automodule:: secml.ml.kernel.c_kernel
   :members:
   :undoc-members:
   :show-inheritance:

CKernelChebyshevDistance
------------------------

.. automodule:: secml.ml.kernel.c_kernel_chebyshev_distance
   :members:
   :undoc-members:
   :show-inheritance:

CKernelEuclidean
----------------

.. automodule:: secml.ml.kernel.c_kernel_euclidean
   :members:
   :undoc-members:
   :show-inheritance:

CKernelHistIntersect
--------------------

.. automodule:: secml.ml.kernel.c_kernel_histintersect
   :members:
   :undoc-members:
   :show-inheritance:

CKernelLaplacian
----------------

.. automodule:: secml.ml.kernel.c_kernel_laplacian
   :members:
   :undoc-members:
   :show-inheritance:

CKernelLinear
-------------

.. automodule:: secml.ml.kernel.c_kernel_linear
   :members:
   :undoc-members:
   :show-inheritance:

CKernelPoly
-----------

.. automodule:: secml.ml.kernel.c_kernel_poly
   :members:
   :undoc-members:
   :show-inheritance:

CKernelRBF
----------

.. automodule:: secml.ml.kernel.c_kernel_rbf
   :members:
   :undoc-members:
   :show-inheritance:

